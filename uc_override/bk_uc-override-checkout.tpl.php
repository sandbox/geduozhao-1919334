<?php
//print_r($variables);
//$form = $variables['element'];
//print_r($form);
global $user;
?>

<div class="uco-checkout">

	<?php /*
	<div class="uco-pane">
  	<?php require_once 'uc-override-checkout-cart.tpl.php'; ?>
  </div>

	<div class="uco-line"></div>
	*/ ?>

	<div style="display:<?php echo $user->uid ? 'none': 'block';?>">
  	<?php require_once 'uc-override-checkout-customer.tpl.php'; ?>
		<div class="uco-line"></div>
  </div>


	<div class="pane-left">
		<div class="uco-pane">
			<?php require_once 'uc-override-checkout-delivery.tpl.php'; ?>
    </div>
  </div>

	<div class="pane-right">
		<div class="uco-pane">
  		<?php require_once 'uc-override-checkout-billing.tpl.php'; ?>
    </div>
  </div>

	<div style="clear:both"></div>
	<div class="uco-line"></div>


	<div class="pane-left">
		<div class="uco-pane">
   		<?php //require_once 'uc-override-checkout-quotes.tpl.php'; ?>
			<?php require_once 'uc-override-checkout-coupon.tpl.php'; ?>
    </div>
    <div class="uco-line"></div>
    <div class="uco-pane">
      <?php require_once 'uc-override-checkout-comments.tpl.php'; ?>
    </div>
  </div>

	<div class="pane-right">
		<div class="uco-pane">
  		<?php require_once 'uc-override-checkout-payment.tpl.php'; ?>
    </div>
  </div>

	<div style="clear:both"></div>
	<div class="uco-line"></div>
 
	<div align="right">
  	<?php echo drupal_render_children($variables['form']['actions']); ?>
  </div>
  
	<div style="display:none">
  	<?php echo drupal_render_children($variables['form']); ?>
  </div>
  

</div>

