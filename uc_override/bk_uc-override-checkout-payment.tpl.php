<?php
//print_r($variables['form']['panes']['payment']);
$variables['form']['panes']['payment']['details']['cc_policy']['#markup'] = '';
?>
<div class="pane-payment">
  <div class="uco-title">
    <?php echo $variables['form']['panes']['payment']['#title'];?>
  </div>
  
  <div class="uco-content">
  	<div style="float:left; width:200px;">
    	<?php echo drupal_render_children($variables['form']['panes']['payment']['details']['cc_number']); ?>
    </div>
  	<div style="float:right; width:100px">
    	<?php echo drupal_render_children($variables['form']['panes']['payment']['details']['cc_cvv']); ?>
    </div>
    <div style="clear:both"></div>
    
    
  <?php 
    echo drupal_render_children($variables['form']['panes']['payment']); 
    unset($variables['form']['panes']['payment']);
  ?>
  </div>
</div>