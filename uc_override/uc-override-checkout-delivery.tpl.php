<div class="pane-delivery">
  <div class="uco-title">
    <?php echo $variables['form']['panes']['delivery']['#title'];?>
  </div>
  
  <div class="uco-content">
  <?php 
    echo drupal_render_children($variables['form']['panes']['delivery']); 
    unset($variables['form']['panes']['delivery']);
  ?>
  </div>
</div>