<?php
//print_r($variables['form']['panes']['coupon']);

//$variables['form']['panes']['coupon']['code']['#description'] = '';
//$variables['form']['panes']['coupon']['coupons']['#description'] = '';
?>

<div id="coupon-pane">
  <div class="uco-title">
    <?php echo $variables['form']['panes']['coupon']['code']['#title'];?>
  </div>
  
  <div class="uco-content">
	<?php 
		//$variables['form']['panes']['coupon']['code']['#title'] = '';
    echo drupal_render_children($variables['form']['panes']['coupon']); 
    unset($variables['form']['panes']['coupon']); 
  ?>
  </div>
</div>