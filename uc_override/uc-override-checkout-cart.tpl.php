<div class="pane-cart">
  <div class="uco-title">
    <?php echo $variables['form']['panes']['cart']['#title'];?>
  </div>
  
  <div class="uco-content">
  <?php 
    echo drupal_render_children($variables['form']['panes']['cart']); 
    unset($variables['form']['panes']['cart']);
  ?>
  </div>
</div>
