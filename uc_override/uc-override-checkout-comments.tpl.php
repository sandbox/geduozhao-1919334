<?php
//print_r($variables['form']['panes']['comments']);
?>

<div class="pane-comments">
  <div class="uco-title">
    <?php echo $variables['form']['panes']['comments']['#title'];?>
  </div>
  
  <div class="uco-content">
  <?php 
		$variables['form']['panes']['comments']['comments']['#title'] = '';
    echo drupal_render_children($variables['form']['panes']['comments']); 
    unset($variables['form']['panes']['comments']);
  ?>
  </div>
</div>