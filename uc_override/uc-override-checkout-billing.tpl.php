<div class="pane-billing">
  <div class="uco-title">
    <?php echo $variables['form']['panes']['billing']['#title'];?>
  </div>
  
  <div class="uco-content">
  <?php 
    echo drupal_render_children($variables['form']['panes']['billing']); 
    unset($variables['form']['panes']['billing']);
  ?>
  </div>
</div>