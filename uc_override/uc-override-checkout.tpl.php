<?php
//print_r($variables);
//$form = $variables['element'];
//print_r($form);
global $user;
?>
<div style="text-align:center; color:#FFF; display:none; "><img src="/subscribe/sites/default/themes/zen/cbi/images/subscribe_checkout_banner.gif" /></div>
    <div class="uco-checkout">
    
        <div class="uco-pane">
            <?php require_once 'uc-override-checkout-cart.tpl.php'; ?>
        </div>
    
        <div class="uco-line"></div>
    
        <div style="display:<?php echo $user->uid ? 'none': 'block';?>">
        <div class="uco-pane">
			<?php require_once 'uc-override-checkout-customer.tpl.php'; ?>
        </div>
        	<div class="uco-line"></div>
      	</div>
    
        
        <div style="clear:both"></div>
    
    
        <div class="uc_box_wrapper" style=" border:#CCC solid; border-width:1px 2px 2px; ">
        
            <div class="pane-left" style="border-style: none; width:51%; padding:0">
                <div class="uco-pane" style="border-style: none;">
                    <?php require_once 'uc-override-checkout-delivery.tpl.php'; ?>
            </div>
          </div>
        
            <div class="pane-right" style="border-style: none; width:49%; padding:0">
                <div class="uco-pane" style="border-style: none;">
                <?php require_once 'uc-override-checkout-billing.tpl.php'; ?>
            </div>
          </div>
          
        <div style="clear:both"></div>
        </div>
    
        <div style="clear:both"></div>
        <div class="uco-line"></div>
    
        <div class="uco-pane">
            <?php require_once 'uc-override-checkout-quotes.tpl.php'; ?>
        </div>
        <div class="uco-line"></div>
        
        <div class="pane-left">
            <div class="uco-pane">
            <?php require_once 'uc-override-checkout-coupon.tpl.php'; ?>
        </div>
        <!--<div class="uco-line"></div>-->
      </div>
    
        <div class="pane-right">
            <div class="uco-pane" >
              <?php require_once 'uc-override-checkout-comments.tpl.php'; ?>
            </div>
        </div>
    
        <div style="clear:both"></div>
    
        <div class="uc_box_wrapper" style="border:#666 solid 1px; margin-bottom:10px; margin-top:10px;">
        
            <div style="border-style: none; width:100%; padding:0">
                <div class="uco-pane" style="border-style: none;">
                    <?php require_once 'uc-override-checkout-payment.tpl.php'; ?>
            </div>
          </div>
    
        </div>
    
    <?php /*?>
        <div class="pane-right">
            <div class="uco-pane">
            <?php require_once 'uc-override-checkout-payment.tpl.php'; ?>
        </div>
      </div>
    
    <?php */?>
    
        <div style="clear:both"></div>
        <div class="uco-line"></div>
     
        <div align="right">
        <?php echo drupal_render_children($variables['form']['actions']); ?>
      </div>
      
        <div style="display:none">
        <?php echo drupal_render_children($variables['form']); ?>
      </div>
      
    
    </div>


<style type="text/css">


.uco-checkout .uco-pane .uco-title {
	background: url(/subscribe/sites/default/themes/zen/cbi/images/search_bar_top.jpg) repeat-x #D6DF22;
	height:27px;
	line-height:27px;
	}
	
.uco-checkout .pane-left {
    float: left;
    padding: 0;
    width: 50%;
}	

.pane-left #delivery-address-pane {
	padding: 0px 15px 15px;
}

#line-items-div {
	text-align:right;
	}
	
#line-items-div table{
	width:100%;
	margin:0;
	}

#line-items-div .title{
	width:90%;
	}
	
#line-items-div .price{
	width:10%;
	}
.form-item label {
	display: inline-block;
	}
	
#payment-details.form-item label {
	width:200px;
	}
	
.uc-cart-checkout-form .uc-store-address-field .form-item label {
    padding: 2px 6px 6px;
    text-align: left;
	font-size:11px;
}

.uc-store-address-field .form-item label {
	width:130px;
}

input {
    border: 1px solid #CCCCCC;
    height: 18px;
    margin-bottom: 1px;
}

#payment-details .form-item label {
    display: inline-block;
    font-weight: bold;
	width:200px;
}

.uco-checkout .uco-pane .uco-content {
    padding: 5px 15px 15px;
}

.form-item-panes-billing-select-address label{
	display:inline;
	}
.form-item-panes-billing-copy-address {
	font-size:11px;
	}
.uco-checkout .pane-delivery #edit-panes-delivery-delivery-zone, .uco-checkout .pane-delivery #edit-panes-delivery-delivery-country, .uco-checkout .pane-billing #edit-panes-billing-billing-zone, .uco-checkout .pane-billing #edit-panes-billing-billing-country {
    width: 234px;
}	
.uco-checkout .pane-delivery, .uco-checkout .pane-billing {
}
#delivery-address-pane input {
	width:230px;
	}
input.form-checkbox, input.form-radio {
    border: none;
    vertical-align: middle;
}
#content .section {
  margin: 10px auto;
  width: 100%;
  padding: 0;
}

#delivery-address-pane .form-required, #billing-address-pane .form-required{
    color: #CC0000;
	float: right;
}
#payment-details .form-item label {
	font-size:11px;
}
.form-item-panes-delivery-select-address {
	padding-top:30px;
	}
</style>
